﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemigo_alien : MonoBehaviour
{
    Rigidbody2D rb;
    public bool dispara = false;
    bala_enemigo bala_GO;
    public gamemode_space_invaders gamemode;

    // Start is called before the first frame update
    void Start()
    {
        rb = transform.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        /*if (dispara)
        {
            transform.GetComponent<SpriteRenderer>().flipY = true;
        }
        else
        {
            transform.GetComponent<SpriteRenderer>().flipY = false;
        }*/
    }

    public void seleccionar_como_disparador()
    {
        dispara = true;
        InvokeRepeating("intentar_disparar", 0, 2);
    }

    public void deseleccionar_como_disparador()
    {
        dispara = false;
        if (IsInvoking())
        {
            CancelInvoke();
        }
    }

    public void intentar_disparar()
    {
        if (dispara == true)
        {
            if (Random.Range(1, 10) > 8)
            {
                disparar();
            }
        }
    }

    private void FixedUpdate()
    {
        
    }

    public void disparar()
    {
        Debug.Log("disparo");
        if (gamemode.judan_no_puru.Count > 0)
        {
            bala_GO = gamemode.judan_no_puru.Dequeue().GetComponent<bala_enemigo>();
            bala_GO.transform.position = transform.position;
            bala_GO.activa = true;
            bala_GO.velocidad = -2;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "bala")
        {
            collision.GetComponent<bala>().salir_de_pantalla();
            transform.parent.transform.GetComponent<disparo_por_ilera>().StartCoroutine("actualizar_muerte");
            Destroy(transform.gameObject);
        }
    }
}
