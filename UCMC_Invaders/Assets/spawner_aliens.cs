﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner_aliens : MonoBehaviour
{

    public List<enemigo_alien> aliens = new List<enemigo_alien>();
    public GameObject ilera;
    public GameObject punto_spawn;
    public List<GameObject> filas_creadas = new List<GameObject>();
    public mover_aliens movimiento_aliens;
    public gamemode_space_invaders gamemode_spawn;

    // Start is called before the first frame update
    void Start()
    {
        gamemode_spawn = FindObjectOfType<gamemode_space_invaders>();
        /*for (int i = 0; i < aliens.Count; i++)
        {
            for (int a = 0; a < 10; a++)
            {
                Instantiate(aliens[i], aliens[i].transform.parent.transform).transform.position = new Vector2(aliens[i].transform.position.x + (a + 1), aliens[i].transform.position.y);
            }
        }*/
        for (int i = 0; i < 11; i++)
        {
            filas_creadas.Add(Instantiate(ilera, transform.parent.transform));
            filas_creadas[i].transform.position = new Vector2(punto_spawn.transform.position.x + 0.25f + (i * 0.748f), punto_spawn.transform.position.y);
            for (int a = 0; a < filas_creadas[i].transform.childCount; a++)
            {
                filas_creadas[i].transform.GetChild(a).GetComponent<enemigo_alien>().gamemode = gamemode_spawn;
            }
        }
        establecer_bounds();
    }

    public void establecer_bounds()
    {
        float posicion_izquierda_temporal = filas_creadas[10].transform.localPosition.x;
        float posicion_derecha_temporal = filas_creadas[0].transform.localPosition.x;
        for (int i = 0; i < filas_creadas.Count; i++)
        {
            if (filas_creadas[i].transform.childCount > 0)
            {
                // mas a la izquierda
                if (filas_creadas[i].transform.localPosition.x < posicion_izquierda_temporal)
                {
                    posicion_izquierda_temporal = filas_creadas[i].transform.localPosition.x;
                }
                // mas a la derecha
                if (filas_creadas[i].transform.localPosition.x > posicion_derecha_temporal)
                {
                    posicion_derecha_temporal = filas_creadas[i].transform.localPosition.x;
                }
                Debug.Log("sep");
            }
        }
        // Aquí pondre el valor de posicion izquierda_temporal en el limite izquierdo
        Debug.Log(posicion_izquierda_temporal);
        // Aquí pondre el valor de posicion izquierda_temporal en el limite izquierdo
        Debug.Log(posicion_derecha_temporal);
        movimiento_aliens.mod_d = posicion_derecha_temporal;
        movimiento_aliens.mod_i = posicion_izquierda_temporal;
        //movimiento_aliens.actualizar();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
