﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mover_aliens : MonoBehaviour
{

    public int direccion = 1;
    public float velocidad = 10;
    public float aceleracion = 0.08f;
    public List<GameObject> bounds = new List<GameObject>();
    public float pos_izquierda;
    public float pos_derecha;
    public float mod_d;
    public float mod_i;

    public float pos_global;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        flotar();
        actualizar();
    }

    public void actualizar()
    {
        float pos = transform.position.x;
        
        pos_derecha = transform.position.x + mod_d;
        pos_izquierda = transform.position.x + mod_i;

    }

    public void flotar()
    {
        if (pos_derecha >= bounds[0].transform.position.x)
        {
            direccion = -1;
            transform.position = new Vector2(transform.position.x, transform.position.y - 0.1f);
        }
        if (pos_izquierda <= bounds[1].transform.position.x)
        {
            direccion = 1;
            transform.position = new Vector2(transform.position.x, transform.position.y - 0.1f);
        }
        transform.position = new Vector2(transform.position.x + velocidad * Time.deltaTime * direccion , transform.position.y);
    }

    public void acelerar()
    {
        velocidad += aceleracion;
    }
}
