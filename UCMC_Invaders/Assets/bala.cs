﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bala : MonoBehaviour
{

    Rigidbody2D rb;
    public int velocidad;
    public bool activa = true;

    // Start is called before the first frame update
    void Start()
    {
        rb = transform.GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(0,velocidad);
    }

    private void OnBecameInvisible()
    {
        activa = false;
        velocidad = 0;
    }

    public void salir_de_pantalla()
    {
        transform.position = new Vector2(3000,3000);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
