﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bala_enemigo : MonoBehaviour
{

    Rigidbody2D rb;
    public int velocidad;
    public bool activa = true;
    gamemode_space_invaders gamemode;

    // Start is called before the first frame update
    void Start()
    {
        rb = transform.GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(0,velocidad);
        gamemode = GameObject.FindObjectOfType<gamemode_space_invaders>();
    }

    private void OnBecameInvisible()
    {
        activa = false;
        velocidad = 0;
        gamemode.judan_no_puru.Enqueue(transform.gameObject);
    }

    public void salir_de_pantalla()
    {
        transform.position = new Vector2(3000,3000);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
