﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gamemode_space_invaders : MonoBehaviour
{
    public int velocidad;
    public Rigidbody2D rb;
    public bala bala_GO;
    public GameObject salida_bala;

    public GameObject[] balas;
    public Queue<GameObject> judan_no_puru = new Queue<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        balas = GameObject.FindGameObjectsWithTag("judan");
        for (int i = 0; i < balas.Length; i++)
        {
            judan_no_puru.Enqueue(balas[i]);
        }
    }

    // Update is called once per frame
    void Update()
    {
        movimiento();
        corregir_posicion();
        if (Input.GetKeyDown(KeyCode.UpArrow)) 
        {
            disparar();
        }
    }

    public void movimiento()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(velocidad, 0);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector2(-velocidad, 0);
        }
        else
        {
            rb.velocity = new Vector2(0, 0);
        }
    }

    public void disparar()
    {
        if (!bala_GO.activa)
        {
            bala_GO.transform.position = salida_bala.transform.position;
            bala_GO.activa = true;
            bala_GO.velocidad = 10;
        }
    }

    public void corregir_posicion()
    {
        transform.position = new Vector2(Mathf.Clamp(transform.position.x, -8.52f, 8.52f), transform.position.y);
    }
}
