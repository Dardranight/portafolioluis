﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disparo_por_ilera : MonoBehaviour
{

    List<GameObject> hijos = new List<GameObject>();
    public spawner_aliens spawn;

    // Start is called before the first frame update
    void Start()
    {
        actualizar_lista();
        designar_disparador();
        spawn = FindObjectOfType<spawner_aliens>();
    }

    // Update is called once per frame
    void Update()
    {
            
    }

    public void actualizar_lista()
    {
        hijos.Clear();
        for (int i = 0; i < transform.childCount; i++)
        {
            hijos.Add(transform.GetChild(i).transform.gameObject);
        }
        if (hijos.Count <= 0)
        {
            Debug.Log("Alguien murio :V");
            spawn.establecer_bounds();
        }
    }

    public void designar_disparador()
    {
        int indice_buscado = -1;
        float posicion_en_lista = hijos[0].transform.position.y;
        for (int i = 0; i < hijos.Count; i++)
        {
            hijos[i].GetComponent<enemigo_alien>().dispara = false;
            if (hijos[i].transform.position.y <= posicion_en_lista)
            {
                posicion_en_lista = hijos[i].transform.position.y;
                indice_buscado = i;
            }
        }
        if (indice_buscado != -1)
        {
            hijos[indice_buscado].GetComponent<enemigo_alien>().seleccionar_como_disparador();
        }
    }

    IEnumerator actualizar_muerte()
    {
        yield return new WaitForSeconds(0.1f);
        actualizar_lista();
        spawn.movimiento_aliens.acelerar();
        if (hijos.Count>0)
        {
            designar_disparador();
        }
    }
}
