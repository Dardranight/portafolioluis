﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class player_controller : MonoBehaviour
{
    #region variables
    public float vida = 10;
    public Rigidbody2D rb;
    SpriteRenderer sprite;
    Animator anim;
    piscina_balas balas;
    float dir_x;
    float dir_y;
    float tiempo_entre_salto;
    int direccion_actual = 1;
    float velocidad_caida;
    float tiempo_entre_disparos;
    float tiempo_entre_colgadas;
    float mod_animacion = 1 ;
    public float cadencia_disparo;
    public float fuerza_salto;
    public float fuerza_salto_derecha;
    public float velocidad;
    bool apuntando;
    bool grounded;
    bool colgado;
    bool supah_salto;
    public bool esperando;
    // Variables del animador
    int velocidad_anim;
    int apuntando_anim;
    int dir_apuntado;
    int grounded_anim;
    int anticipacion;
    int recepcion;
    int velocidad_caida_anim;
    int colgado_anim;
    // Variables para detectar cosas
    public Vector3 offset_pies;
    public Vector3 offset_pies_2;
    int mascara_suelo;
    // Variables para sistema de escalado
    public Vector3 offset_rayo1;
    public Vector3 offset_rayo2;
    public Vector3 offset_rayo3;
    public Vector3 offset_colgado;
    public Vector3 offset_teletransporte;
    // Variables del disparo
    GameObject forward_GO;
    GameObject backward_GO;
    GameObject upward_GO;
    GameObject upward_back_GO;
    GameObject downward_GO;
    GameObject downward_back_GO;
    Vector3 downward_point;
    Quaternion downward_angle;
    // Variables de la granada
    public int cantidad_granadas = 1;
    public GameObject prefab_granada;
    // Sistema_dialogos
    public dialog_system dialogos;
    // Check Point
    #endregion 
    // Start is called before the first frame update
    void Start()
    {
        vida = PlayerPrefs.GetFloat("vida_jugador",2);
        rb = transform.GetComponent<Rigidbody2D>();
        sprite = transform.GetComponent<SpriteRenderer>();
        anim = transform.GetComponent<Animator>();
        balas = FindObjectOfType<piscina_balas>();
        velocidad_anim = Animator.StringToHash("velocidad");
        apuntando_anim = Animator.StringToHash("apuntando");
        dir_apuntado = Animator.StringToHash("dir_apuntado");
        mascara_suelo = LayerMask.NameToLayer("ground");
        mascara_suelo = LayerMask.GetMask("ground");
        grounded_anim = Animator.StringToHash("grounded");
        anticipacion = Animator.StringToHash("anticipacion");
        velocidad_caida_anim = Animator.StringToHash("velocidad_caida");
        colgado_anim = Animator.StringToHash("colgado");
        tiempo_entre_disparos = Time.time;
        tiempo_entre_colgadas = Time.time;
        forward_GO = transform.GetChild(0).transform.gameObject;
        backward_GO = transform.GetChild(3).transform.gameObject;
        upward_GO = transform.GetChild(1).transform.gameObject;
        upward_back_GO = transform.GetChild(4).transform.gameObject;
        downward_GO = transform.GetChild(2).transform.gameObject;
        downward_back_GO = transform.GetChild(5).transform.gameObject;
        Debug.Log(SceneManager.GetActiveScene().name);
        if (SceneManager.GetActiveScene().name != "Nivel0")
        {
            string check;
            check = PlayerPrefs.GetString("Nivel0", "Vacio");
            
            if (check == "Vacio")
            {
                return;
            }
            else
            {
                List<string> informacion = new List<string>();
                string[] data_raw = check.Split('/');
                transform.position = new Vector3(float.Parse(data_raw[0]),float.Parse(data_raw[1]),float.Parse(data_raw[2]));
                vida = float.Parse(data_raw[3]);
                cantidad_granadas = int.Parse(data_raw[4]);
            }
        }
        else
        {

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (vida <= 0)
        {
            return;
        }
        leer_entrada();
        colgarse();
        detectar_suelo();
        animar();
    }

    private void FixedUpdate()
    {
        if (esperando)
        {
            return;
        }
        movimiento();
    }

    public void leer_entrada()
    {
        velocidad_caida = rb.velocity.y;
        if (esperando)
        {
            return;
        }
        if (!grounded)
        {
            return;
        }
        dir_x = Input.GetAxisRaw("Horizontal");
        if (!colgado)
        {
            if (dir_x > 0)
            {
                direccion_actual = 1;
                sprite.flipX = false;
            }
            if (dir_x < 0)
            {
                direccion_actual = -1;
                sprite.flipX = true;
            }
        }
        dir_y = Input.GetAxisRaw("Vertical");
        // Accion apuntar
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
            apuntando = !apuntando;
        }
        if (Input.GetButtonDown("Fire1"))
        {
            disparar();
        }
        if (Input.GetButtonDown("Fire2"))
        {
            lanzar_granada();
        }
        // Accion saltar
        if (Input.GetButtonDown("Jump") && !apuntando && !colgado)
        {
            if (Time.time< tiempo_entre_salto)
            {
                return;
            }
            if (Mathf.Abs(rb.velocity.x) >0.2f)
            {
                salto();
                return;
            }
            esperando = true;
            anim.Play(anticipacion);
            Invoke("salto", 0.4f);
        }
    }

    public void animar()
    {
        anim.SetFloat(velocidad_anim,Mathf.Abs(dir_x * mod_animacion));
        anim.SetBool(apuntando_anim, apuntando);
        anim.SetFloat(dir_apuntado, dir_y);
        anim.SetBool(grounded_anim, grounded);
        anim.SetBool(colgado_anim, colgado);
        anim.SetFloat(velocidad_caida_anim, velocidad_caida);
    }

    public void disparar()
    {
        if (Time.time < tiempo_entre_disparos)
        {
            return;
        }
        if (!apuntando)
        {
            return;
        }
        tiempo_entre_disparos = Time.time + cadencia_disparo;
        if (dir_y > 0)
        {
            anim.Play("shot_upward");
        }
        if (dir_y < 0)
        {
            anim.Play("shot_downward");
        }
        if (dir_y == 0)
        {
            anim.Play("shot_forward");
        }
    }

    public void dispararo(int version)
    {
        if (version == 1)
        {
            if (direccion_actual > 0)
            {
                balas.solicitar_bala(forward_GO.transform.position, direccion_actual, forward_GO.transform.rotation);
            }
            else
            {
                balas.solicitar_bala(backward_GO.transform.position, direccion_actual, backward_GO.transform.rotation);
            }
        }
        if (version == 2)
        {
            if (direccion_actual > 0)
            {
                balas.solicitar_bala(upward_GO.transform.position, direccion_actual, upward_GO.transform.rotation);
            }
            else
            {
                balas.solicitar_bala(upward_back_GO.transform.position, direccion_actual, upward_back_GO.transform.rotation);
            }
        }
        if (version == 3)
        {
            if (direccion_actual > 0)
            {
                balas.solicitar_bala(downward_GO.transform.position, direccion_actual, downward_GO.transform.rotation);
            }
            else
            {
                balas.solicitar_bala(downward_back_GO.transform.position, direccion_actual, downward_back_GO.transform.rotation);
            }
        }
    }

    public void lanzar_granada()
    {
        if (Time.time < tiempo_entre_disparos)
        {
            return;
        }
        if (!apuntando)
        {
            return;
        }
        tiempo_entre_disparos = Time.time + cadencia_disparo;
        anim.Play("launch");
    }

    public void lanzamiento_granada()
    {
        if (cantidad_granadas > 0)
        {
            granada grana;
            if (direccion_actual == 1)
            {
                grana = Instantiate(prefab_granada, transform.GetChild(7).transform.position, new Quaternion(0, 0, 0, 0)).transform.GetComponent<granada>();
            }
            else
            {
                grana = Instantiate(prefab_granada, transform.GetChild(8).transform.position, new Quaternion(0, 0, 0, 0)).transform.GetComponent<granada>();
            }
            grana.impulsar(direccion_actual);
            cantidad_granadas -= 1;
        }
    }

    public void movimiento()
    {
        if (apuntando)
        {
            return;
        }
        if (colgado)
        {
            return;
        }
        if (!grounded)
        {
            rb.velocity = new Vector2(Mathf.Clamp(rb.velocity.x, -20, 20), Mathf.Clamp(rb.velocity.y, -20, 10));
            return;
        }
        if (Time.time < tiempo_entre_salto)
        {
            return;
        }
        rb.velocity = new Vector2(dir_x * velocidad, Mathf.Clamp(rb.velocity.y,-10,10));
    }

    public void salto()
    {
        esperando = false;
        if (apuntando)
        {
            return;
        }
        if (!grounded)
        {
            return;
        }
        tiempo_entre_salto = Time.time + 1;
        if (supah_salto)
        {
            rb.AddForce(transform.up * fuerza_salto * 10, ForceMode2D.Impulse);
        }
        else
        {
            rb.AddForce(transform.up * fuerza_salto + transform.right * 1f * direccion_actual, ForceMode2D.Impulse);
        }
    }

    public void colgarse()
    {
        if (Time.time < tiempo_entre_colgadas)
        {
            return;
        }
        if (colgado)
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                rb.bodyType = RigidbodyType2D.Dynamic;
                colgado = false;
                tiempo_entre_colgadas = Time.time + .5f;
            }
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                anim.SetTrigger("subir");
                StartCoroutine("teleport_retardado", 0.8f);
                tiempo_entre_colgadas = Time.time + 0.8f;
            }
            return;
        }

        RaycastHit2D rayo1;
        RaycastHit2D rayo2;
        RaycastHit2D rayo3;

        rayo1 = trazar_rayo_bool(transform.position + new Vector3(offset_rayo1.x * direccion_actual , offset_rayo1.y), Vector2.right * direccion_actual, true, 1f, mascara_suelo, .1f);
        rayo2 = trazar_rayo_bool(transform.position + new Vector3(offset_rayo2.x * direccion_actual, offset_rayo2.y), Vector2.right * direccion_actual, true, 1f, mascara_suelo, .1f);
        supah_salto = rayo2;
        rayo3 = trazar_rayo_bool(transform.position + new Vector3(offset_rayo3.x * direccion_actual, offset_rayo3.y), Vector2.down, true, 1f, mascara_suelo, .1f);

        if (rayo1 == true & rayo2 == false & rayo3 == true)
        {
            Vector3 pos = transform.position;
            if (direccion_actual > 0)
            {
                pos.x += (rayo1.distance - .05f);
            }
            else
            {
                pos.x -= (rayo1.distance + .05f);
            }
            pos.y -= (rayo3.distance);
            pos += new Vector3(offset_colgado.x * direccion_actual,offset_colgado.y);
            transform.position = pos;
            rb.bodyType = RigidbodyType2D.Static;
            colgado = true;
        }
    }

    public void teletransportarse(Vector3 posicion, GameObject tarjet)
    {
        tarjet.transform.position = posicion;
    }

    public void detectar_suelo()
    {
        if (colgado)
        {
            return;
        }
        RaycastHit2D pie1 = trazar_rayo_bool(transform.position - offset_pies, Vector3.up * -1, true, .2f, mascara_suelo, 1f); ;
        RaycastHit2D pie2 = trazar_rayo_bool(transform.position + offset_pies_2, Vector3.up * -1, true, .2f, mascara_suelo, 1f); ;
        if (pie1 || pie2)
        {
            grounded = true;
        }
        else
        {
            grounded = false;
        }
    }

    public void recibir_dano()
    {
        vida -= 1;
        anim.Play("pain");
        if (vida <= 0)
        {
            vida = 0;
            rb.velocity = Vector3.zero;
        }
    }

    public RaycastHit2D trazar_rayo_bool(Vector3 posicion_inicial, Vector3 posicion_final, bool Dibujable, float Distancia_maxima, int Mascara, float Duracion)
    {
        RaycastHit2D resultado;
        resultado = Physics2D.Raycast(posicion_inicial, posicion_final,Distancia_maxima, Mascara);
        if (resultado.collider != null)
        {
            Debug.DrawRay(posicion_inicial, posicion_final*Distancia_maxima, Color.green, Duracion);
            return resultado;
        }
        Debug.DrawRay(posicion_inicial, posicion_final*Distancia_maxima, Color.red, Duracion);
        return resultado;
    }

    IEnumerator teleport_retardado(float tiempo)
    {
        Vector3 pos = transform.position;
        transform.position += Vector3.up * 4.278f;
        yield return new WaitForSeconds(tiempo);
        colgado = false;
        transform.position = pos;
        teletransportarse(transform.position + new Vector3(offset_teletransporte.x * direccion_actual,offset_teletransporte.y), transform.gameObject);
        anim.SetTrigger("ended");
        rb.bodyType = RigidbodyType2D.Dynamic;
        tiempo_entre_colgadas = Time.time + .5f;
    }
}
