﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class piscina_balas : MonoBehaviour
{

    Queue<bala> cargador = new Queue<bala>();
    List<GameObject> balas_existentes;
    public GameObject prefab;
    public Vector3 posicion_para_guardar;
    public bala bala_recien_creada;

    // Start is called before the first frame update
    void Awake()
    {
        for (int i = 0; i < 1; i++)
        {
            bala_recien_creada = Instantiate(prefab, posicion_para_guardar, new Quaternion(0, 0, 0, 0)).transform.GetComponent<bala>();
            bala_recien_creada.pool_bullet = this;
            solicitar_cola(bala_recien_creada);
        }    
    }

    public void solicitar_cola(bala bala_pendiente)
    {
        cargador.Enqueue(bala_pendiente);
        bala_pendiente.gameObject.transform.position = posicion_para_guardar;
        bala_pendiente.gameObject.transform.gameObject.SetActive(false);
    }

    public void solicitar_bala(Vector3 pos, int dir, Quaternion rot)
    {
        if (cargador.Count > 0)
        {
            bala bala;
            bala = cargador.Dequeue();
            bala.transform.position = pos;
            bala.transform.rotation = rot;
            bala.direccion = dir;
            bala.transform.gameObject.SetActive(true);
        }
        else
        {
            bala bala_handle = Instantiate(prefab, posicion_para_guardar, rot).GetComponent<bala>();
            bala_handle.pool_bullet = this;
            solicitar_cola(bala_handle);
            solicitar_bala(pos, dir, rot);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
