﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class piscina_objetos : MonoBehaviour
{

    Queue<GameObject> enemigos = new Queue<GameObject>();
    public GameObject enemigo_prefab;

    // Start is called before the first frame update
    void Awake()
    {
        GameObject enemigo_recien_instanciado;
        for (int i = 0; i < 5; i++)
        {
            enemigo_recien_instanciado = Instantiate(enemigo_prefab, Vector3.zero, new Quaternion(0, 0, 0, 0)).transform.gameObject;
            enemigo_recien_instanciado.GetComponent<enemigo_1>().pool_propia = this;
            solicitar_cola(enemigo_recien_instanciado);
        }
    }

    public void solicitar_cola( GameObject objeto)
    {
        enemigos.Enqueue(objeto);
        enemigo_1 solicitante = objeto.GetComponent<enemigo_1>();
        solicitante.vida = 3;
        solicitante.vivo = true;
        solicitante.transform.GetComponent<BoxCollider2D>().enabled = true;
        objeto.SetActive(false);
    }

    public void solicitar_enemigo(Vector3 posicion, int dir, float agro)
    {
        if (enemigos.Count > 0)
        {
            enemigo_1 enemigo_temporal;
            enemigo_temporal = enemigos.Dequeue().GetComponent<enemigo_1>();
            enemigo_temporal.transform.position = posicion;
            enemigo_temporal.transform.rotation = new Quaternion(0,0,0,0);
            enemigo_temporal.direccion = dir;
            enemigo_temporal.distancia_deteccion = agro;
            enemigo_temporal.transform.gameObject.SetActive(true);
        }
        else
        {
            enemigo_1 enemigo_handler = Instantiate(enemigo_prefab, Vector3.zero, new Quaternion(0, 0, 0, 0)).GetComponent<enemigo_1>();
            enemigo_handler.pool_propia = this;
            solicitar_cola(enemigo_handler.transform.gameObject);
            solicitar_enemigo(posicion, dir, agro);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
