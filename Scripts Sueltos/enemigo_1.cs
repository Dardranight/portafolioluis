﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemigo_1 : MonoBehaviour
{
    #region variables
    public int vida = 3;
    public float distancia_deteccion;
    public int direccion = 1;
    public bool vivo = true;
    hit_stop stop;
    Animator anim;
    Rigidbody2D rb;
    public float velocidad_movimiento;
    float velocidad_original;
    int muerto;
    int velocidad;
    int player;
    bool caminando;
    float tiempo_para_paso_siguiente;
    // variables daño
    public GameObject posicion_para_dano;
    public GameObject posicion_para_dano_negativo;
    public float distancia;
    float tiempo_entre_ataques;
    public float cadencia_ataque;
    bool libre;
    bool detenido = true;
    bool player_al_alcance = false;
    SpriteRenderer sprite;
    player_controller player_enemigo;
    float distancia_con_jugador;
    public piscina_objetos pool_propia;
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        stop = FindObjectOfType<hit_stop>();
        anim = transform.GetComponent<Animator>();
        rb = transform.GetComponent<Rigidbody2D>();
        sprite = transform.GetComponent<SpriteRenderer>();
        player_enemigo = FindObjectOfType<player_controller>();
        muerto = Animator.StringToHash("Muerto");
        velocidad = Animator.StringToHash("Velocidad");
        player = LayerMask.GetMask("Player");
        tiempo_entre_ataques = Time.time;
        velocidad_original = velocidad_movimiento;
        InvokeRepeating("cambiar_desicion", 2, 2);
    }

    // Update is called once per frame
    void Update()
    {
        animar();
        if (!vivo)
        {
            return;
        }
        cerebro();
    }

    public void cerebro()
    {
        detectar_player();
        calcular_distancia_con_player();
        perseguir();
        if (!libre)
        {
            return;
        }
        if (detenido)
        {
            return;
        }
        caminar_sencillo();
    }

    public void detectar_player()
    {
        RaycastHit2D enemigo;
        enemigo = trazar_rayo_bool(transform.position + transform.right * direccion + Vector3.up * 4, Vector3.right * direccion, true, 1.6f, player, .1f);
        if (enemigo)
        {
            libre = false;
            if (Time.time < tiempo_entre_ataques)
            {
                return;
            }
            anim.Play("enemigo_ataque");
            tiempo_entre_ataques = Time.time + cadencia_ataque;
        }
        else
        {
            libre = true;
        }
    }

    public void calcular_distancia_con_player()
    {
        distancia_con_jugador = Vector3.Distance(transform.position,player_enemigo.transform.position);
        if (distancia_con_jugador < distancia_deteccion)
        {
            player_al_alcance = true;
            velocidad_movimiento = velocidad_original * 2;
        }
        else
        {
            player_al_alcance = false;
            velocidad_movimiento = velocidad_original;
        }
    }

    public void die_lov(Transform pos)
    {
        if (transform.position.x > pos.position.x)
        {
            direccion = -1;
        }
        else
        {
            direccion = 1;
        }
        vivo = false;
        anim.Play("die_lov");
    }

    public void practicar_la_suicidacion()
    {
        recibir_daño();
        recibir_daño();
        recibir_daño();
    }

    public void caminar_sencillo()
    {
        rb.velocity = new Vector3(velocidad_movimiento * direccion, rb.velocity.y);
    }

    public void atacar_sencillo()
    {
        RaycastHit2D resultado_ataque;
        if (direccion > 0)
        {
            resultado_ataque = Physics2D.Raycast(posicion_para_dano.transform.position, Vector2.right, distancia);
            Debug.DrawRay(posicion_para_dano.transform.position, Vector2.right * distancia, Color.red, .1f);
        }
        else
        {
            resultado_ataque = Physics2D.Raycast(posicion_para_dano_negativo.transform.position, Vector2.left, distancia);
            Debug.DrawRay(posicion_para_dano_negativo.transform.position, Vector2.left * distancia, Color.red, .1f);
        }
        if (resultado_ataque)
        {
            if (resultado_ataque.transform.CompareTag("Player"))
            {
                player_controller player;
                player = resultado_ataque.transform.GetComponent<player_controller>();
                player.recibir_dano();
            }
        }
    }

    public void recibir_daño()
    {
        anim.Play("enemigo_pain");
        stop.parar(.1f);
        vida -= 1;
        if (vida < 1)
        {
            vivo = false;
            rb.velocity = new Vector3(0, rb.velocity.y);
            rb.bodyType = RigidbodyType2D.Static;
            distancia_deteccion = 15;
            transform.GetComponent<BoxCollider2D>().enabled = false;
        }
    }

    public void animar()
    {
        float velocidad_temporal;
        velocidad_temporal = Mathf.Ceil(Mathf.Abs(rb.velocity.x));
        anim.SetBool(muerto, vivo);
        anim.SetFloat(velocidad, velocidad_temporal);
        if (direccion == 1)
        {
            sprite.flipX = true;
        }
        else
        {
            sprite.flipX = false;
        }
    }

    public void cambiar_desicion()
    {
        if (!vivo)
        {
            return;
        }
        if (!libre)
        {
            return;
        }
        if (player_al_alcance)
        {
            return;
        }
        float decicion = Random.Range(0f, 1f);
        if (decicion < .3f)
        {
            detenido = true;
        }
        if (decicion < .6f & decicion >.3f)
        {
            detenido = false;
            direccion = 1;
        }
        if (decicion < 1f & decicion > .6f)
        {
            detenido = false;
            direccion = -1;
        }
    }

    public void perseguir()
    {
        if (!player_al_alcance)
        {
            return;
        }
        if (player_enemigo.transform.position.x >= transform.position.x)
        {
            detenido = false;
            direccion = 1;
        }
        if (player_enemigo.transform.position.x < transform.position.x)
        {
            detenido = false;
            direccion = -1;
        }
    }

    public void desactivar()
    {
        rb.bodyType = RigidbodyType2D.Dynamic;
        pool_propia.solicitar_cola(transform.gameObject);
    }

    public RaycastHit2D trazar_rayo_bool(Vector3 posicion_inicial, Vector3 posicion_final, bool Dibujable, float Distancia_maxima, int Mascara, float Duracion)
    {
        RaycastHit2D resultado;
        resultado = Physics2D.Raycast(posicion_inicial, posicion_final, Distancia_maxima, Mascara);
        if (resultado.collider != null)
        {
            Debug.DrawRay(posicion_inicial, posicion_final * Distancia_maxima, Color.green, Duracion);
            return resultado;
        }
        Debug.DrawRay(posicion_inicial, posicion_final * Distancia_maxima, Color.red, Duracion);
        return resultado;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, distancia_deteccion);
    }

}
