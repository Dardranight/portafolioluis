﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Llenador : MonoBehaviour
{
    public Sprite[] sprites;
    public HashSet<Sprite> ListaVolcada = new HashSet<Sprite>();
    public HashSet<Sprite> ListaVolcada2 = new HashSet<Sprite>();
    public List<Sprite> ListaVolcada_List = new List<Sprite>();
    public List<Sprite> ListaVolcada2_List = new List<Sprite>();
    public int maximo = 18;

    public GameObject instancia;
    // Start is called before the first frame update
    void Start()
    {
        //Raw_Texturas = (Resources.LoadAll("Imagenes") as Texture2D);
        /*for (int i = 0; i < Raw_Texturas.Length; i++)
        {
            Sprites.Add(Sprite.Create(Texturas[i], new Rect(0, 0, Texturas[i].width, Texturas[i].height), new Vector2(0.5f, 0.5f)));
        }*/
        //Texturas.Add(Resources.Load("Imagenes/1") as Texture2D);
        //sprite = Sprite.Create(Texturas[0], new Rect(0, 0, Texturas[0].width, Texturas[0].height), new Vector2(0.5f, 0.5f));
        //sprite = Resources.Load<Sprite>("Imagenes/1");
        sprites = (Resources.LoadAll<Sprite>("Imagenes"));
        while (ListaVolcada.Count < 18)
        {
            ListaVolcada.Add(sprites[Random.Range(0, sprites.Length)]);
        }
        while (ListaVolcada2.Count < 18)
        {
            ListaVolcada2.Add(sprites[Random.Range(0, sprites.Length)]);
        }
        foreach (Sprite spr in ListaVolcada)
        {
            ListaVolcada_List.Add(spr);
        }
        foreach (Sprite spr in ListaVolcada2)
        {
            ListaVolcada2_List.Add(spr);
        }
        for (int i = 0; i < ListaVolcada_List.Count; i++)
        {
            crear_carta(ListaVolcada_List[i]);
            crear_carta(ListaVolcada2_List[i]);
        }
    }

    public void crear_carta(Sprite imagen)
    {
        GameObject temporal;
        temporal = Instantiate(instancia,transform);
        temporal.GetComponent<Funcionalidad_Carta>().Default_Frontal[1] = imagen;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
