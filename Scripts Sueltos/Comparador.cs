﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Comparador : MonoBehaviour
{

    public List<GameObject> Comparables = new List<GameObject>();
    public int contador = 0;
    public AudioSource bocina;
    public AudioClip vfx_rotar;

    // Start is called before the first frame update
    public void Recibir_carta( GameObject Carta)
    {
        Comparables[contador] = Carta;
        contador += 1;
        if (contador == 2)
        {
            Comparar_GO();
        }
    }

    public void Comparar_GO()
    {
        contador = 0;
        if (Comparables[0].GetComponent<Image>().sprite == Comparables[1].GetComponent<Image>().sprite)
        {
            Debug.Log("Son Iguales");
        }
        else
        {
            foreach (GameObject Carta in Comparables)
            {
                Carta.GetComponent<Funcionalidad_Carta>().Tapar_carta();
            }
            Debug.Log("Son Diferentes");
        }
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
